import {ClubSchema} from '../model/ClubInformation.js';

import express from 'express';

let router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.post('/register', (req, res, next) => {
    
});

router.post('/update', (req, res, next) => {
    
});

router.post('/delete', (req, res, next) => {
    
});

router.get('/getSponsoredByLocation', (req, res, next) => {
    
});



module.exports = router;