import {ClubModel, ClubPhotoModel, ClubReviewModel} from '../model/ClubInformation.js';
import {PartyModel} from '../model/PartyInformation.js';
import {Const} from '../Utils/Const.js';

import passport from 'passport';
import express from 'express';
import assert from 'assert';
import mongoose from 'mongoose';
import multer from 'multer';
import path from 'path';


import {needGroups} from '../middlewares/groupMiddleware.js';
import passportLocal from 'passport-local';

import ensureLogin from 'connect-ensure-login';

let ensureClubLoggedIn = ensureLogin.ensureLoggedIn;

//Connect to local Mongoose DB.

// async  getClub(username)    {
//     return await findClubUser(username);
// }


//Path relative to www path.


let storage = multer.diskStorage({
  destination:  (req, file, cb) => {
    cb(null, './public/uploads/parties');
  },
  filename:  (req, file, cb) => {
    if(typeof req.body.username !== 'undefined' )   {
        cb(null, req.body.username + "_" + file.fieldname + "_" + Date.now() + path.extname(file.originalname));
    }
    else {
        cb(null, file.fieldname + "_" + Date.now() + path.extname(file.originalname))
    }
    
  },
  fileFilter : (req, file, cb) => {
     
    if(path.extname(file.originalname) == ".jpg" || 
        path.extname(file.originalname) == ".jpeg" || 
        path.extname(file.originalname) == ".png" ||
        path.extname(file.originalname) == ".gif")    {
        cb(null,true);
    }
    else {
        cb(null,false);
    }

  }
});


let upload = multer({ storage : storage });


let router = express.Router();



/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});


router.get('/register', needGroups('admins|clubs'), (req, res, next) => {
    res.render('parties/register.jade', {title : 'Register'});
});

router.post('/register', needGroups('admin|clubs'), upload.single('photo'), (req, res, next)  => {
    let party = new PartyModel();
    party.party_type = Const.PARTY_TYPE_CLUB;
    party.start_date = req.body.start_date;
    party.end_date = req.body.end_date;
    party.name = req.body.name;
    party.lat = req.body.lat;
    party.lon = req.body.lon;
    party.club_id = req.user._id;
    
    
    let photo = new ClubPhotoModel();
    photo.club_id = req.user._id;
    photo.photo_url = req.file.path;
    photo.photo_is_user_tagged = false;
    photo.save();
    
    party.photo = photo._id;
    party.description = req.body.description;
    party.going = 0;
    party.invited = 0;
    party.total_seats = req.body.total_seats;
    party.booked_seats = 0;
    
    party.save();
    console.log("Party Saved with ID : " + party._id);  
    res.redirect('/parties');
    
});

router.get('/update', needGroups('admins|clubs'), (req, res, next)  => {
    res.send('Not Implemented Yet.');
});

router.post('/update', needGroups('admins|clubs'), (req, res, next) => {
    
});

router.post('/delete', needGroups('admins|clubs') , (req, res, next) => {
    //Get club_id corresponding to prty ID.
    // if(isAdmin(req.user._id))   {
    //     PartyModel.removePartyById(req.body.party_id, (err) {
    //         if(err) {
    //             res.send('Error removing Party');
    //         }  
    //         else {
    //             res.send('Party with ID : ' + req.body.party_id + ' removed');
    //         }
    //     });
    // }
    // else {     
    // }
    
    // ONLY HERE FOR HISTORICAL REASON, *Pssshh* to remind me how stupid I was.
    // NOPE , no security issue whatsoever, JUST MAKE THE SECRET LONG ENOUGH OK ? 
    // Security Issue : Should we depend solely on sessions ? 
    // like if someone is able to steal a large amount of session ids of groups
    // this middleware of needGroups fails as it deserializes session_id( _id ) to user account.
    // oh wait that's what session.secret is for *silly me*, anyhow the system is pretty secure
    // just need to make the session.secret more random.
    // this comment is for historical reasons, please don't remove it in future.
    
    PartyModel.findById(req.body.party_id, (err, party) => {
            if(err) {
                res.send('Some weird error encountered');
            }
            //TODO : this can be optimized by removing the .populate in PartyInformation.statics.findById
            else if(party.club_id._id.toString() == req.user._id.toString())   {
                PartyModel.removePartyById(req.body.party_id, (err) => {
                    if(err) {
                        res.send('Error removing Party');
                    }  
                    else {
                        res.send('Party with ID : ' + req.body.party_id + ' removed');
                    }
                });
                
            }
            else {
                res.send(401, 'You are not authorized to use this commands.');
            }
        });
});

router.get('/getPartyByClubName', (req,res, next) => {
    
});


router.get('/getPartyByLocation', (req, res, next)  => {
    let lat = typeof req.query.lat === 'undefined' ? 23.2599333 : req.query.lat;
    let lon = typeof req.query.lon === 'undefined' ? 77.41261499999996 : req.query.lon;
    let radius = typeof req.query.radius === 'undefined' ? 300 : req.query.radius;
    let page = typeof req.query.page !== 'undefined' ? Math.max(0, req.query.page) : 0;
    let limit = typeof req.query.limit !== 'undefined' ? Math.max(1, req.query.limit) : 10;
    
    PartyModel.findByLatLonRadius(lat, lon, radius, page, limit, (err, parties)  => {
        if(err) {
            console.log(err);
            res.send("No parties found in nearby locations");
        }
        else {
            res.json(parties);    
        }
        console.log(parties);
        
    });
});


/**
 * order of registeration matters 
 * if we put this up at top all /* will be overriden 
 * like if you call /clubs/register /:id will be called instead.
 * make usre to put these at the end of file.
 */

router.get('/:id', (req, res, next) => {
    PartyModel.findById(req.params.id, (err, party) => {
        if(err) {
            res.send(err);
        }
        else if(party == null) {
            res.send("Party ID Invalid");
        }
        else {
            res.json(party);
        }
    });
});

module.exports = router;