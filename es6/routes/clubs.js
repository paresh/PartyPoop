import {ClubModel, ClubPhotoModel} from '../model/ClubInformation.js';

import passport from 'passport';
import express from 'express';
import assert from 'assert';
import mongoose from 'mongoose';
import multer from 'multer';
import path from 'path';

import {needGroups} from '../middlewares/groupMiddleware.js';

import passportLocal from 'passport-local';

import ensureLogin from 'connect-ensure-login';

let ensureClubLoggedIn = ensureLogin.ensureLoggedIn;


let LocalStrategy = passportLocal.Strategy;

passport.use('club-stratergy', new LocalStrategy(async (username,password,done) => {
    let club = undefined;
    console.log("[Authenticating]");
    try {
        club = await findClubUser(username);
    }
    catch(err)  {
        console.log(err);
        return done(null,false, {message : "Unexpected shutdown"});
    }
    
    if(typeof club !== 'undefined' && typeof club.password !== 'undefined' && club.password == password)    {
       
       return done(null, club);
    }
    else if(typeof club !== 'undefined' && typeof club.password === 'undefined')    {
        return done(null,false, { message : "Incorrect Username or Password"});
    }
    else {
        return done(null,false, { message : "Incorrect Username or Password"});
    }
}));

// async  getClub(username)    {
//     return await findClubUser(username);
// }

let findClubUser = (username)  => {
    
    return new Promise((resolve, reject)  =>  {
        ClubModel.findByUsername( username, (err, club) => {
                console.log(err);
                console.log(club);
                if(err) {
                    //handle Error
                    return reject(err)
                }
                else {
                    resolve(club)
                }
            });
    });

};


//Path relative to www path.


let storage = multer.diskStorage({
  destination:  (req, file, cb) => {
    cb(null, './public/uploads/clubs');
  },
  filename:  (req, file, cb) => {
    if(typeof req.body.username !== 'undefined' )   {
        cb(null, req.body.username + "_" + file.fieldname + "_" + Date.now() + path.extname(file.originalname));
    }
    else {
        cb(null, file.fieldname + "_" + Date.now() + path.extname(file.originalname))
    }
    
  },
  fileFilter : (req, file, cb)  => {
     
    if(path.extname(file.originalname) == ".jpg" || 
        path.extname(file.originalname) == ".jpeg" || 
        path.extname(file.originalname) == ".png" ||
        path.extname(file.originalname) == ".gif")    {
        cb(null,true);
    }
    else {
        cb(null,false);
    }

  }
});


let upload = multer({ storage : storage });


let router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.render('clubs/index', { title: 'Express', isLoggedIn : typeof req.user !== 'undefined' });
});


router.get('/login', (req,res,next) => {
    if(req.user)    {
        switch(req.user.group)  {
            case 'user':
                res.redirect('/users');
                break;
            case 'club':
                res.redirect('/clubs');
                break;
            case 'admin':
                res.redirect('/admin');
                break;
           case 'sponsored':
                res.redirect('/sponsored');
                break;
           default:
                res.redirect('/');
                
        }
        
    }
    else {
        res.render('clubs/login');
    }
});

router.post('/login', passport.authenticate('club-stratergy', { successRedirect : '/clubs' , failureRedirect : '/clubs/login', successFlash : true, failureFLash : true}));

router.get('/logout', needGroups('clubs'),(req, res)=> {
    req.logout();
    res.redirect('/clubs');
});

router.get('/register', (req,res,next)  => {
    if(req.user)    {
        res.redirect('/clubs');
    }
    else {
        res.render('clubs/register', {title : 'Club Registeration'})
    }
    
});


router.post('/register', upload.single('profile_photo') , (req, res)   => {
    //Store club Data.
    let club = new ClubModel();
    club.name = req.body.name;
    club.username = req.body.username;
    club.password = req.body.password;
    club.email = req.body.email;
    club.contact = req.body.contact;
    club.lat = req.body.lat;
    club.lon = req.body.lon;
    club.description = req.body.description;
    club.days_open = req.body.days_open;
    club.opening_time = req.body.opening_time;
    club.closing_time = req.body.closing_time;
    club.our_rating = 0;
    club.user_rating = 0;
    
    club.enabled = true;
    
    console.log(req.body);
    
    //Store uploaded image and get their Id.
    // let clubPhotosId = [];
    let clubId = club._id;
    //fix images if necessary.
    //TODO : add minimum image size check.
    let photo = new ClubPhotoModel();
    photo.club_id = clubId;
    photo.photo_url = req.file.path;
    photo.photo_is_user_tagged = false;
    photo.save();
    //clubPhotosId.push(photo._id);
    console.log(req.file);
    //set club photos
    //club.photos = clubPhotosId;
    club.photos = [];
    club.profile_photo = photo._id;
    club.reviews = [];
    
    club.save();
    
    res.redirect("/clubs/login");
    
});

router.get('/update', needGroups('admins|clubs'), (req, res)  => {
    res.render('clubs/update.jade', { title : "Update Club Information", isLoggedIn : typeof req.user !== 'undefined'});
});

router.post('/update', needGroups('admins|clubs'), (req, res) => {
    res.send('WIP');
});



router.post('/delete', needGroups('admins|clubs'), (req, res) => {
    ClubModel.findById(req.body.club_id, (err, club) => {
            if(err) {
                res.send('Some weird error encountered');
            }
            else {
                club.enabled = false;
                club.save();
                res.send('Club disabled with ID ' + req.body.club_id + ' (because deleting it will break a lot of stuff).');
            }
        });
});

router.get('/getClubById/:id', (req, res) => {
    if(typeof req.params.id !== 'undefined')    {
        ClubModel.findById(req.params.id, (err, club)   => {
            if(err) {
                console.log(err);
                res.send("No Club with ID : " + req.params.id);
            } 
            else {
                res.json(club);
            }
        });
    }
    else {
        res.send("No Club with ID : " + req.params.id);
    }
    
});

router.get('/getClubByUsername/:username', (req, res) => {
    if(typeof req.params.username !== 'undefined')    {
        ClubModel.findByUsername(req.params.username, (err, club) => {
            if(err) {
                console.log(err);
                res.send("No Club with Username : " + req.params.username);
            } 
            else {
                res.json(club);
            }
        });
    }
    else {
        res.send("No Club with Username : " + req.params.username);
    }
});

//TODO : fix this.
router.get('/getClubByName/:name', (req, res) => {
    if(typeof req.params.name !== 'undefined')    {
        let page = typeof req.query.page !== 'undefined' ? Math.max(0, req.query.page) : 0;
        let limit = typeof req.query.limit !== 'undefined' ? Math.max(1, req.query.limit) : 10;
        ClubModel.findByName(req.params.name, page, limit, (err, clubs) => {
            if(err) {
                console.log(err);
                res.send("No Club with Name similar to : " + req.params.name);
            } 
            else {
                res.json(clubs);
            }
        });
    }
    else {
        res.send("No Club with Name similar to : " + req.params.name);
    }
});

router.get('/getClubsByLocation', (req, res) => {
    
    let lat = typeof req.query.lat === 'undefined' ? 23.2599333 : req.query.lat;
    let lon = typeof req.query.lon === 'undefined' ? 77.41261499999996 : req.query.lon;
    let radius = typeof req.query.radius === 'undefined' ? 300 : req.query.radius;
    let page = typeof req.query.page !== 'undefined' ? Math.max(0, req.query.page) : 0;
    let limit = typeof req.query.limit !== 'undefined' ? Math.max(1, req.query.limit) : 10;
    
    ClubModel.findByLatLonRadius(lat, lon, radius, page, limit, (err, clubs) => {
        if(err) {
            console.log(err);
            res.send("No clubs found in nearby locations");
        }
        else {
            res.json(clubs);    
        }
        console.log(clubs);
        
    });
});


router.get('/:id', (req, res, next) => {
    if(typeof req.params.id !== 'undefined')    {
        ClubModel.findById(req.params.id, (err, club) => {
            if(err) {
                console.log(err);
                res.send("No Club with ID : " + req.params.id);
            } 
            else {
                res.json(club);
            }
        });
    }
    else {
        res.send("No Club with ID : " + req.params.id);
    }
});

module.exports = router;