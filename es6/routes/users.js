import {ClubModel, ClubPhotoModel} from '../model/ClubInformation.js';
import {UserModel, LikedClubMode, VisitedClubModel, UserRefferedModel} from '../model/UserInformation.js';

import {needGroups} from '../middlewares/groupMiddleware.js';

import passport from 'passport';
import express from 'express';
import assert from 'assert';
import mongoose from 'mongoose';
import multer from 'multer';
import path from 'path';

import passportLocal from 'passport-local';

import ensureLogin from 'connect-ensure-login';

let ensureUserLoggedIn = ensureLogin.ensureLoggedIn;

//Connect to local Mongoose DB in app.js

let LocalStrategy = passportLocal.Strategy;

passport.use('user-stratergy', new LocalStrategy(async (username, password, done) => {
    let user = undefined;
    console.log("[Authenticating]");
    try {
        user = await findUser(username);
    }
    catch(err)  {
        console.log(err);
        return done(null,false, {message : "Unexpected shutdown"});
    }
    
    if(typeof user !== 'undefined' && typeof user.password !== 'undefined' && user.password == password)    {
       return done(null, user);
    }
    else if(typeof user !== 'undefined' && typeof user.password === 'undefined')    {
        return done(null,false, { message : "Incorrect Password"});
    }
    else {
        return done(null,false, { message : "Incorrect Password"});
    }
}));

// async  getClub(username)    {
//     return await findClubUser(username);
// }

let findUser = (username) => {
    
    return new Promise((resolve, reject) => {
        UserModel.findOne(
            {'username' : username}, 
            (err, club) => {
                if(err) {
                    //handle Error
                    return reject(err)
                }
                else {
                    resolve(club)
                }
            }
        )
    });

};


//Path relative to www path.


let storage = multer.diskStorage({
  destination:  (req, file, cb) => {
    cb(null, './public/uploads/users');
  },
  filename:  (req, file, cb) => {
    if(typeof req.body.username !== 'undefined' )   {
        cb(null, req.body.username + "_" + file.fieldname + "_" + Date.now() + path.extname(file.originalname));
    }
    else {
        cb(null, file.fieldname + "_" + Date.now() + path.extname(file.originalname))
    }
    
  },
  fileFilter : (req, file, cb) => {
     
    if(path.extname(file.originalname) == ".jpg" || 
        path.extname(file.originalname) == ".jpeg" || 
        path.extname(file.originalname) == ".png" ||
        path.extname(file.originalname) == ".gif")    {
        cb(null,true);
    }
    else {
        cb(null,false);
    }

  }
});


let upload = multer({ storage : storage });

let router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/login', (req, res, next) => {
    if(req.user)    {
        switch(req.user.group)  {
            case 'user':
                res.redirect('/users');
                break;
            case 'club':
                res.redirect('/clubs');
                break;
            case 'admin':
                res.redirect('/admin');
                break;
           case 'sponsored':
                res.redirect('/sponsored');
                break;
           default:
                res.redirect('/');
                
        }
        
    }
    else {
        res.render('users/login');
    }
});

router.post('/login', passport.authenticate('user-stratergy', { successRedirect : '/users' , failureRedirect : '/users/login', successFlash : true, failureFLash : true}));

router.get('/logout', needGroups('users'), (req, res) => {
    req.logout();
    res.redirect('/users');
});


router.get('/register', (req, res, next) => {
    res.render('users/register', { title : 'User Registeration' });
});

router.post('/register', (req, res, next) => {
    
});

router.post('/update', needGroups('admins|users'), (req, res, next) => {
    
});

router.get('/getUserById/:id', needGroups('developer|admins|users'), (req, res, next) => {
    
});


router.get('/getUserByUserName/:username', needGroups('developers|admins|users'), (req, res, next) => {
    
});

router.get('/:id', (req, res, next) => {
    
});

module.exports = router;
