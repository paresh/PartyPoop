import express from 'express';
import assert from 'assert';

let router = express.Router();

/* GET home page. */
export default router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});



module.exports = router;