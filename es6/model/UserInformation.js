import mongoose from 'mongoose';

let Schema = mongoose.Schema;

/**
 * Password : Use bcrypt with nonce to store passwords.
 */

let UserSchema = new Schema ({
    user_name : String,
    password : String,
    nonce : String,
    group : { type : String, default : "users"},
    enabled : { type : Boolean, default : true },
    user_title : String,
    name : String,
    contact_no : String,
    email : String,
    fantasy_points : Number,
    profile_photo : String,
    cover_photo : String,
    gender : String,
    location : String,
    current_location : String,
    refferal_id : String
});

let LikedClubsSchema = new Schema ({
    user_id : { type : Schema.Types.ObjectId , refs : 'User' },
    club_id : { type : Schema.Types.ObjectId, ref : 'Club' }
});

let VisitedClubsSchema = new Schema ({
    user_id : { type : Schema.Types.ObjectId , refs : 'User' },
    club_id : { type : Schema.Types.ObjectId, ref : 'Club' },
    club_verified : { type : Boolean, default : false },
    user_verified : { type : Boolean, default : false}
});

let UserRefferedSchema = new Schema ({
    user_id : { type : Schema.Types.ObjectId , refs : 'User' },
    user_reffered_user_id : { type : Schema.Types.ObjectId , refs : 'User' },
    refferal_date : { type : Date, default : Date.now }
});

export let UserModel = mongoose.model('User', UserSchema);
export let LikedClubModel = mongoose.model('Liked', UserSchema);
export let VisitedClubModel = mongoose.model('Visisted', UserSchema);
export let UserRefferedModel = mongoose.model('Reffered', UserSchema);