//API Key for developers who want to build 3rd party applications.
//use needGroups('developer') to use the API Key functionality in API's.

import mongoose from 'mongoose';

let Schema = mongoose.Schema;


let APIKeySchema = new Schema ({
    email_id : String,
    api_key : String,
    enabled : { type : Boolean, default : true },
    request_count : { type : Number, default : 0 },
    request_quota : { type : Number, default : 10000 }
});

APIKeySchema.statics.findByKey = (apiKey, cb) => {
    return this.find( { api_key : apiKey } )
                .exec(cb);
};

export let APIKeyModel = mongoose.model('APIKey', APIKeySchema);