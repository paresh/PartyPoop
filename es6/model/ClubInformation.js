import mongoose from 'mongoose';

let Schema = mongoose.Schema;

/**
 * group :
 *  group can have following values 
 *  0 -> root / admin
 *  1 -> clubs
 *  2 -> users
 *  3 -> sponsers
 * 
 *  for now we leave this to be of type String but do note this can be optimized later on
 *  as I don't know as of know which direction this project is going ???
 *  can we make it big or die trying ...
 * 
 *  TODO : Decide on nonce size ??
 * 
 *  use bcrypt with nonce to store passwords.
 */

let ClubSchema = new Schema ({
    name : String,
    username : String,
    password : String,
    nonce : String,
    group : { type : String, default : "clubs"},
    enabled : { type : Boolean, default : true },
    email : String,
    contact : String,
    address : String,
    lat : Number,
    lon : Number,
    description : String,
    days_open : String,
    opening_time : String,
    closing_time : String,
    our_rating : Number,
    user_rating : Number,
    profile_photo : { type : Schema.Types.ObjectId, ref : 'ClubPhoto' },
    photos : [ { type : Schema.Types.ObjectId, ref : 'ClubPhoto' } ],
    reviews : [ { type : Schema.Types.ObjectId, ref : 'ClubReview' } ]
}); 

ClubSchema.statics.findById = (id, cb) => {
    return this.findOne( { _id : id })
                .populate('profile_photo')
                .exec(cb);
};

ClubSchema.statics.findByUsername = (username, cb) => {
    return this.findOne( { username : username }, cb);
};

ClubSchema.statics.findByName = (name, page, limit, cb) => {
    let skip = page * limit;
    return this.find( {name : new RegExp('^' + name + '$', "i")})
                .limit(limit)
                .skip(skip)
                .exec(cb);
}

/**
 * Returns points inside the box in which the circle is inscribed with radius = width of box / 2.
 * Can be refined using points which satisfy (x - center) ^ 2  + (y - center) ^ 2 < radius ^ 2
 * 
 *     -----------------------
 *     |                     |
 *     |                     |
 *     |     (LAT,LON)       |
 *     |         *-----------|
 *     |           (RADIUS)  |
 *     |                     |
 *     |_____________________|
 * 
 * The approximate conversions are:
 *
 * Latitude: 1 deg = 110.574 km
 *
 * Longitude: 1 deg = 111.320*cos(latitude) km
 * 
 * Source : http://stackoverflow.com/questions/1253499/simple-calculations-for-working-with-lat-lon-km-distance
 * 
 */
ClubSchema.statics.findByLatLonRadius = (lat, lon, radius, page, limit, cb) => {
    let unitLat = 111.574;
    let unitLon = Math.abs(111.320 * Math.cos(lat));
    console.log("1Km = " + 1/unitLat + " Latitudes 1Km = " + 1/unitLon + " Longitudes");
    var radLat = radius/unitLat;
    var radLon = radius/unitLon;
    console.log("Radius in latitudes = " + radLat);
    console.log("Radius in longitudes = " + radLon);
    
    let skip = page * limit;
    
    return this.find( { } )
            .where('lat').gt(lat - radLat).lt(lat + radLat)
            .where('lon').gt(lon - radLon).lt(lon + radLon)
            .where('enabled').equals('true')
            .populate('profile_photo')
            .limit(limit)
            .skip(skip)
            .exec(cb);
};

let ClubPhotosSchema = new Schema ({
    club_id : Schema.Types.ObjectId,
    photo_url : String,
    photo_is_user_tagged : Boolean
});

ClubPhotosSchema.statics.findById = (id, cb) => {
    return this.findOne( { _id : id }, cb);  
};

ClubPhotosSchema.statics.findByClubId = (id, cb) => {
    return this.find( { club_id : id }, cb);
};

let ClubReviewsSchema = new Schema ({
    club_id : Schema.Types.ObjectId,
    photos_id : [Schema.Types.ObjectId],
    review_description : String
});

ClubReviewsSchema.statics.findById = (id, cb) => {
    return this.findOne( { _id : id }, cb);
};

ClubReviewsSchema.statics.findByClubId = (id, cb) => {
    return this.find( { club_id : id }, cb);
};


export let ClubModel = mongoose.model('Club', ClubSchema);
export let ClubPhotoModel = mongoose.model('ClubPhoto', ClubPhotosSchema);
export let ClubReviewModel = mongoose.model('ClubReview', ClubReviewsSchema);