import mongoose from 'mongoose';
import {ClubModel} from './ClubInformation';

let Schema = mongoose.Schema;

let PartySchema = new Schema ({
    party_type : Number,
    enabled : { type : Boolean, default : true },
    start_date : Date,
    end_date : Date,
    name : String,
    lat : Number,
    lon : Number,
    club_id : { type : Schema.Types.ObjectId, ref : 'Club' },
    photo : { type : Schema.Types.ObjectId, ref : 'ClubPhoto' },
    description : String,
    interested : Number,
    going : Number,
    invited : Number,
    total_seats : Number,
    booked_seats : Number
});

PartySchema.statics.findById = (id, cb) => {
    return this.findOne( { _id : id } )
            .populate("club_id photo")
            .exec(cb);
};

PartySchema.static.findByClubId = (id, cb) => {
    return this.find( { club_id : id } )
            .populate("club_id photo")
            .exec(cb);
};

PartySchema.statics.findByLatLonRadius = (lat, lon, radius, page, limit, cb) => {
    let unitLat = 111.574;
    let unitLon = Math.abs(111.320 * Math.cos(lat));
    console.log("1Km = " + 1/unitLat + " Latitudes 1Km = " + 1/unitLon + " Longitudes");
    var radLat = radius/unitLat;
    var radLon = radius/unitLon;
    console.log("Radius in latitudes = " + radLat);
    console.log("Radius in longitudes = " + radLon);
    
    let skip = page * limit;
    return this.find()
            .where('lat').gt(lat - radLat).lt(lat + radLat)
            .where('lon').gt(lon - radLon).lt(lon + radLon)
            .populate('cllub_id photo')
            .limit(limit)
            .skip(skip)
            .exec(cb);
};

PartySchema.statics.removePartyById = (id, cb) => {
    return this.findOne({ '_id' : id })
                .remove()
                .exec(cb);
};

export let PartyModel = mongoose.model('Party', PartySchema);