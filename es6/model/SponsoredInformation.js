import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let SponsoredSchema = new Schema ({
    name : String,
    description : String,
    sponsored_image : String,
    location : String,
    start_date : Date,
    end_date : Date,
    enabled : { type : Boolean, default : true }
});

export let SponsoredModel = mongoose.model('Sponsored', SponsoredSchema);