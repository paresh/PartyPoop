/**
 * matches provided `group` with Model.group
 * verifies user group which is currently signed in.
 * 
 * 
 * for developer group check the API Key provided.
 */

import {APIKeyModel} from '../model/APIKeyInformation.js';

export let needGroups = function(groups) {
  return function(req, res, next) {
        groups = groups.toLowerCase();
        let grps = groups.split("|");
        let isDeveloperAllowed = groups.indexOf('developers') != -1 ? true : false;
        if (req.user)   {
            var success = false;
            for( let group of grps ) {
                if(group === req.user.group)    {
                    console.log('Success');
                    next();
                    success = true;
                    break;
                }
            }
            if(!success)    {
                //Could be a developer check API Key.
                // if(typeof req.query.api_key !== 'undefined'  && isDeveloperAllowed)    {
                //     APIKeyModel.findByKey(req.query.api_key, function(err, developer)   {
                //         //check request quota here ? 
                //         if(err) {
                //             res.send(500, 'Service Unavailable');
                //         }
                //         else if(typeof developer !== 'undefined') {
                //             next();
                //         }
                //     });
                // }
                // else {
                //     res.send(401, 'Unauthorized');
                // }
                res.send(401, 'Unauthorized');
            }
        }
        else    {
            if(typeof req.query.api_key !== 'undefined'  && isDeveloperAllowed)    {
                APIKeyModel.findByKey(req.query.api_key, function(err, developer)   {
                    //check request quota here ? 
                    if(err) {
                        res.send(500, 'Service Unavailable');
                    }
                    else if(typeof developer !== 'undefined') {
                        next();
                    }
                });
            }
            else {
                res.send(401, 'Unauthorized');
            }
            
            res.send(401, 'Unauthorized');
        }
  };
};