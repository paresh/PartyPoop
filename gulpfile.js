var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var path = require("path");

var mongoose = require('mongoose');

var config = require('./config.json');

//Gulp plugins

var del = require('del');
var install = require("gulp-install");
var taskListing = require('gulp-task-listing');
var notify = require('gulp-notify');

var sourceRoot = path.join(__dirname, 'es6');
var es6Path = "es6/**/*.js"
var distPath = "dist"
 
// Add a task to render the output 

gulp.task('watch', function() {
    gulp.watch(es6Path, ['babel']);
});

gulp.task('default', taskListing);


gulp.task('build', ['build:uploads', 'build:dependencies', 'build:dist']);

gulp.task('build:dist', function()  {
    return gulp.src(es6Path)
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write(".",{ sourceRoot: sourceRoot }))
        .pipe(gulp.dest(distPath));
});

gulp.task('build:dependencies', ['build:bower', 'build:npm']);

gulp.task('build:bower', function() {
    return gulp.src('./public/bower.json')
        .pipe(install({ 
            production : config.production,
            args: ['--allow-root']
        }));
});

gulp.task('build:npm', function()   {
    return gulp.src('./package.json')
       .pipe(install( { production : config.production
       }));
});

gulp.task('build:uploads', function()   {
    return gulp.src('./public/readme.txt')
        .pipe(gulp.dest('./public/uploads'))
        .pipe(gulp.dest('./public/uploads/clubs'))
        .pipe(gulp.dest('./public/uploads/parties'))
        .pipe(gulp.dest('./public/uploads/users'))
        .pipe(gulp.dest('./public/uploads/sponsored'));
    
});

gulp.task('clean', ['clean:uploads', 'clean:dependencies', 'clean:dist']);

gulp.task('clean:uploads', function()   {
    console.info('Cleaning uploads.')
    return del([
        './public/uploads',
    ]);
});

gulp.task('clean:dependencies', function()   {
    console.info('Cleaning dependencies.');
    return del([
        //Delete's del so how do I use del ? and other modules ofcourse  
        // './node_modules',
        './public/bower_components'
    ]);
});

gulp.task('clean:dist', function()  {
    console.info('Cleaning Distribution, run gulp babel to rebuild dist/ files');
    return del([
        './dist'
    ]);
});

gulp.task('clean:database', function()  {
    mongoose.connect(config.dbAddress, function()  {
        mongoose.connection.db.dropDatabase();
    });
    return;
});