"use strict";
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var mongoose = require('mongoose');

var uploads = multer({ dest: 'public/uploads/'});

require('source-map-support').install();



//Unified model.

//Connect to local Mongoose DB.
mongoose.connect("mongodb://mongo/PartyPoop");

var routes = require('./dist/routes/index');
var users = require('./dist/routes/users');
var parties = require('./dist/routes/parties');
var clubs = require('./dist/routes/clubs');
var sponsored = require('./dist/routes/sponsored');
var passport = require('passport');
var flash = require('express-flash');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret : 'some1337Secret' , 
                    store : new MongoStore( { url : "mongodb://mongo/PartyPoop"} )
                    }));
app.use(flash());

/**
 * configure middlewares.
 */

app.use(passport.initialize());
app.use(passport.session());

//Now user here could be a club or user.

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  let ClubModel = require('./dist/model/ClubInformation.js').ClubModel;
  ClubModel.findById(id, function(err, club) {
    done(err, club);
  });
});


app.use('/', routes);
app.use('/users', users);
app.use('/parties', parties);
app.use('/clubs', clubs);
app.use('/sponsored', sponsored);





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
