FROM node:latest

MAINTAINER Paresh Chouhan <pareshchouhan2013@gmail.com>

ADD ./ /app

WORKDIR /app

# RUN npm config set loglevel warn
# RUN npm install --silent
# RUN npm install -g gulp bower --silent
# RUN gulp build
RUN chmod a+x wait-for-it.sh

EXPOSE 3000

# indefinietly till mongo container is up and running mongodb.
CMD ["./wait-for-it.sh -t 0 mongo:27017"]
CMD ["node", "www"]